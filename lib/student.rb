class Student
  attr_accessor :first_name, :last_name

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @enrolled_courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def courses
    @enrolled_courses
  end

  def enroll(course)
    if @enrolled_courses.any? {|enrolled| enrolled.conflicts_with?(course)}
      raise "course conflict"
    elsif !@enrolled_courses.include?(course)
      @enrolled_courses << course
      course.students << self
    end
  end

  def course_load
    load = Hash.new(0)
    @enrolled_courses.each {|course| load[course.department] += course.credits}
    load
  end
end
